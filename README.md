# Solus Stunts - Car Racing Game

[![Godot 4.0.alpha](Godot-v4.0.svg)](https://downloads.tuxfamily.org/godotengine/4.0/)
[![MIT license](License-MIT-blue.svg)](LICENSE)

Available on

<a href='https://hugegameartgd.itch.io/solus-stunts'>itch.io</a>, 
<a href='https://gamejolt.com/games/solus-stunts/376335'>gamejolt</a>, 
<a href='https://hugegameartgd.indiegala.com/solus-stunts/'>indiegala</a>, 
<a href='https://www.indiedb.com/games/solus-stunts'>indiedb</a>, 
<a href='https://www.moddb.com/games/solus-stunts'>moddb</a>, 
<a href='https://www.indiexpo.net/de/games/solus-stunts'>indiexpo</a>, 
<a href='https://lutris.net/games/solus-stunts/'>lutris</a>

This is the "official" repository of the Solus Stunts Multiplayer Project (made with Godot 3.x).

It features a simple car racing game with five maps.

#### Compiling from source

[See the Solus-Stunts wiki](https://codeberg.org/HugeGameArtGD/Solus-Stunts/wiki)
for compilation instructions and planned features.

![Screenshot](source/images/screen.jpg)